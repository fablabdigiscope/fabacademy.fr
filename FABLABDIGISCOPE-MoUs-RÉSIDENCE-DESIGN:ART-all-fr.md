## Fablab Digiscope: Memorandum of Understanding (MoU) pour les Designer en Résidence temporaire, les Artistes en Résidence temporaire, les Acteurs des Industries Créatives en Résidence temporaire, les Porteurs de Projets Technologiques Open-Source/Open Hardware en Résidence temporaire
###### Catégorie d’abonnement: `Aucune. Les candidats bénéficient de l'accès aux équipements et du conseil de l'équipe du Fablab Digiscope à titre gratuit en échange de contre-parties présentées ci-dessous`
###### Publics: `Tous les publics résidents`

---

#### *Fablab Digiscope est une plateforme dédiée au développement de prototypes pour la recherche, pour l'éducation, et pour la formation. Fablab Digiscope propose des résidences de fabrication ou de rehcerche aux publics extérieurs, pour des projets professionnels. __On le sait moins, Fablab Digiscope propose de temps à autres des résidences aux Designers et Artistes et Hackers qui parategent ses valeurs__. Ce "Memorandum Of Understanding" rassemble les règles qui permettent de bénéficier de l'accompagnement de l'équipe du Fablab Digiscope ainsi que de l'accès à ses équipements. A lire attentivement !*
<!--### Fablab Digiscope is platform dedicated to the development of prototypes for research, education and vocational training. During their journey at université Paris-Saclay, students of every level are allowed to use Fablab Digiscope for their professional or personal project. This Memorandum of Understanding sets the rules for benefiting of the the support of the Fablab Digiscope's Team and for the use of its very complete equipment. Please read carefully-->

---

#### L’utilisation du Fablab pour vos travaux de fabrication et de recherche est favorisée par l'attribution d'un statut de résident temporaire au Fablab Digiscope. Celle-ci requiert d’établir un Memorandum Of Understanding préalable pour assurer le meilleur support de la part du Fablab Manager ainsi que la meilleure expérience pour vous, en tant qu’utilisateur du Fablab Digiscope. Il est donc essentiel de définir ce cadre, d’où l’importance de lire attentivement et d’adhérer à ce MoU, en préalable au démarrage de vos activités de recherche au Fablab Digiscope.
#### Nous vous demanderons également de lire et de signer la Charte Internationale des Fablabs.

---

###### Au cours de notre expérience avec des publics aussi doués que divers, nous avons pu constater qu'il est important de faire ici la distinction entre la notion de "service" et la notion de "support". En effet, celle-ci est subtile, et il nous revient de la clarifier ici pour vous afin de vous accueillir dans les meilleures conditions. Voici quelques précisions qui vous permettront d'aborder au mieux l'utilisation du Fablab Digiscope | LISN | UPSACLAY:
+ Les espaces et les équipements de Fablab Digiscope sont à votre service. Néanmoins, ce n'est pas parce que vous êtes résident que votre responsabilité dans la bonne tenue des machines et des espaces du fablab s'efface. En tant que résident à titre gratuit, vous êtes également responsable du bon fonctionnement du fablab. Merci de laisser les machines et les espaces - au minimum - dans l'état dans lequel vous les avez trouvé. Prenez le temps de regarder la machine avant de l'utiliser, et signalez toute dégradation ou situation anormale au Fablab Manager.

+ Le Fablab Manager ou l’équipe du fablab peuvent répondre à votre demande de support dans la mesure où vous aurez su anticiper votre demande. L'équipe du Fablab Digiscope n'est pas à votre service. Rappelez-vous toujours qu'une "Urgence de votre côté ne constitue pas nécessairement une urgence de notre côté". Un RDV doit être pris au préalable pour vous assurer de notre disponibilité en cas de besoin.

---

###### Pour commencer votre travail de recherche au Fablab Digiscope
+ Solliciter le service du Fablab Digiscope commence par la participation à nos formations sur les technologie(s) qui vous intéressent pour votre résidence, puis par la réservation de votre premier créneau pour commencer vos travaux en (presque) autonomie.

---

###### Pour commencer votre travail de recherche au Fablab Digiscope
+ Solliciter le support-recherche du Fablab Manager du Fablab Digiscope vous demandera plus d’organisation. Il est important de comprendre que si vos travaux sont intéressants pour nous, de nombreuses autres activités et publics sont déjà en interaction avec notre équipe et mobilisent nos équipements sur des temps très cadrés qui laissent peu de place à l’improvisation.

---

###### Pour solliciter notre support-recherche ( 1 mois avant votre besoin), nous vous recommandons de procéder à quelques opérations de management de projet avant de nous présenter votre projet:
+ 1-De planifier votre recherche en nous présentant un rétro-planning (Gant ou autre)
+ 2-De préparer un BOM (Bill of Material) pour anticiper les coûts liés à votre projet
+ 3-De préparer un croquis/un mock-up/une projection qui permet de saisir les caractéristiques conceptuelles et techniques de votre projet
+ 4-De préparer une liste de liens web qui permet de situer votre projet par rapport à des recherches similaires

---

###### Au cours du développement de votre projet, nous vous demanderons de:
+ -Formaliser la Documentation pour la compréhension de l’équipe du fablab
+ -Réserver formellement les temps de support-conseil avec le Fablab Manager
+ -Réserver les créneaux-machine comme les autres l’utilisateurs sur un agenda dédié à la recherche et au support-recherche
+ -D'anticiper vos besoins en matériaux, en les achetant à l'avance

---

###### Ce que vous pouvez attendre de l’espace Fablab Digiscope:
+ -L’accès autonome aux équipements du lundi au vendredi de 10h à 18h sur réservation
+ -Des machines en ordre de marche
+ -Un endroit pour poser votre ordinateur portable et laisser quelques affaires
###### Ce que vous pouvez attendre de l’équipe de Fablab Digiscope:
+ -Les conseils en Design Appliqué à la Fabrication Numérique de la part de notre équipe (sur rendez-vous)
+ -La formation à la compréhension et à l’utilisation des équipements, des machines, et des protocoles de fabrication numérique.

---

###### Ce que Fablab Digiscope attend de vous:
+ -Une participation encadrée par l'équipe du Fablab Digiscope aux activités quotidiennes et in situ du Fablab
+ -Un retour communautaire via la tenue de 1 à 3 Master Class selon la durée de votre résidence.
+ -Que vous évacuiez les déchets dont vous êtes responsables en-dehors du bâtiment
+ -Que vous co-assuriez la maintenance des machines que vous utilisez pour vos recherches
+ -Que vous citiez Fablab Digiscope | LISN | UPSACLAY dans vos communications

---

###### Conditions particulières d'exclusion du Fablab Digiscope
+ -Certains équipements de Fablab Digiscope requièrent une attention accrue. A titre d'exemple, démarrer la découpeuse laser en oubliant d'allumer le système d'extraction des gaz est clairement un motif d'exclusion.
+ -Le constat de dégradations sur une machine peut entraîner l'exclusion d'un utilisateur.
+ -Tout comportement constaté e considéré comme mettant en péril la santé ou la vie d'autrui entraînera un exclusion.
+ -Toute forme d'insulte ou de harcèlement avéré entraînera l'exclusion définitive.
+ -En période de pandémie, le non-respect délibéré et répété des gestes-barrières et des distances de sécurité pourra entraîner l'exclusion.
+ -L'absence de masque dans l'enceinte du fablab engendrera l'obligation de quitter les lieux pour s'en procurer un.

---

###### Rappel très important: Une urgence de votre cote ne constitue pas une urgence de notre côté

---

###### Nom du résident :

---

###### Qualité du résident :

---

###### Email du résident :

---

###### Numéro de téléphone du résident :

---

###### Date :

---

###### Signature du résident :

---

###### Signature du Fablab Manager :

---

###### Signature du Directeur du Fablab Digiscope :
