---
title: FAB ACADEMY FRANCE
subtitle:
comments: false
---

### La Fab Academy Hexagone ou Fab Academy France regroupe les fablabs français qui opèrent la formation d'éducation distribuée FAB ACADEMY. Cette formation avancée en fabrication numérique est le standard international en matière de maîtrise d'un fablab et en matière d'acquisition de connaissances en design appliqué à la fabrication numérique.
