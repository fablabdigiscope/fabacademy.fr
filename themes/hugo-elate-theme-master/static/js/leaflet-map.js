
var planes = [
		["Fablab UPSACLAY",48.7124, 2.16722],
		["La Machinerie",49.8921572, 2.3088106],
    ["Agrilab UniLaSalle", 49.467703, 2.072737],
		["FabLab Sorbonne Université", 48.84654406858142, 2.3548655148865714],
		["Technistub", 47.761588540083736, 7.359660653963101]
		];

        var map = L.map('map').setView([46, 2], 5.45);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';

        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 18,
            }).addTo(map);

		for (var i = 0; i < planes.length; i++) {
			marker = new L.marker([planes[i][1],planes[i][2]])
				.bindPopup(planes[i][0])
				.addTo(map);
		}
//https://gist.github.com/d3noob/9150014
